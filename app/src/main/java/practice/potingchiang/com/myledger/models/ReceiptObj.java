package practice.potingchiang.com.myledger.models;

import java.io.Serializable;

/**
 * This is the receipt model
 * Created by potingchiang on 2017-07-02.
 */

public class ReceiptObj implements Serializable {

    // order number
    private Integer orderNumber;
    // year
    private String  year;
    // month
    private String  month;
    // date
    private String  date;
    // location
    private String  location;
    // subtotal
    private Float   subtotal;
    // tax
    private Float   tax;
    // total
    private Float   total;

    // new instance
    public static ReceiptObj newInstance() {

        return new ReceiptObj();
    }
    // getter & setter
    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Float getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Float subtotal) {
        this.subtotal = subtotal;
    }

    public Float getTax() {
        return tax;
    }

    public void setTax(Float tax) {
        this.tax = tax;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }
}
