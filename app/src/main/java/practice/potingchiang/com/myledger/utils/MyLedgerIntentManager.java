package practice.potingchiang.com.myledger.utils;

import android.content.Context;
import android.content.Intent;

import practice.potingchiang.com.myledger.models.ReceiptObj;
import practice.potingchiang.com.myledger.utils.interfaces.IMyLedgerIntent;
import practice.potingchiang.com.myledger.views.LandingActivity;
import practice.potingchiang.com.myledger.views.NewReceiptActivity;

import static practice.potingchiang.com.myledger.utils.MyLedgerIntentManager.IntentKeys.RECEIPT_OBJECT;

/**
 * IntentManager is to handle all the intent in the app
 * Created by potingchiang on 2017-07-03.
 */

public class MyLedgerIntentManager implements IMyLedgerIntent<ReceiptObj> {

    public static final class IntentKeys {

        public static final String RECEIPT_OBJECT = "RECEIPT_OBJECT";
    }

    // context
    private Context context;

    // constructor
    public MyLedgerIntentManager(Context context) {
        this.context = context;
    }

    // landing
    public void toLandingAc() {

        context.startActivity(
                new Intent(context, LandingActivity.class)
        );
    }
    // add new receipt
    public void toNewReceiptAc(ReceiptObj data) {

        // init
        Intent myIntent = new Intent(context, NewReceiptActivity.class);
        // add info
        myIntent.putExtra(RECEIPT_OBJECT, data);
        // start
        context.startActivity(myIntent);
    }
}
