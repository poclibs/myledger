package practice.potingchiang.com.myledger.presenters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentActivity;

import pandaz.library.potingchiang.com.mvp.presenter.IPresenter;
import practice.potingchiang.com.myledger.db.MyLedgerDbHelper;
import practice.potingchiang.com.myledger.db.contract.MyLedgerContract;
import practice.potingchiang.com.myledger.db.manager.MyLedgerDbManager;
import practice.potingchiang.com.myledger.db.manager.ReceiptDbCrudManager;
import practice.potingchiang.com.myledger.models.ReceiptObj;
import practice.potingchiang.com.myledger.views.fragments.NewReceiptFragment;

import static practice.potingchiang.com.myledger.utils.MyLedgerIntentManager.IntentKeys.RECEIPT_OBJECT;

/**
 * This is the presenter for adding a new receipt
 * Created by potingchiang on 2017-07-02.
 */

public class NewReceiptPresenter implements IPresenter<NewReceiptFragment, ReceiptDbCrudManager> {


    // inner interface for interface
    public interface IAddNewReceiptPresenter {

        // init & bind data
        void bindWithPresenter();
        // getter & setter
        // order number
        Integer getOrderNumber();
        void setOrderNumber(Integer orderNumber);
        // year
        String getYear();
        void setYear(String year);
        // month
        String getMonth();
        void setMonth(String month);
        // date
        String getDate();
        void setDate(String date);
        // location
        String getLocation();
        void setLocation(String location);
        // subtotal
        float getSubtotal();
        void setSubtotal(Float subtotal);
        // tax
        float getTax();
        void setTax(Float tax);
        // total
        float getTotal();
        void setTotal(Float total);
    }

    // context
    private FragmentActivity    myActivity;
    // viewClass
    private NewReceiptFragment  viewClass;
    // db manager
    private MyLedgerDbManager myLedgerDbManager;
    // db open helper
    private MyLedgerDbHelper    myLedgerDbHelper;
    // db
    private SQLiteDatabase      db;
    // modelManager
    private ReceiptDbCrudManager modelDbManager;
    // corresponding model
    private ReceiptObj myReceipt;

    /**
     * constructor with no param
     */
    public NewReceiptPresenter() {

        this.viewClass      = new NewReceiptFragment();
        this.myActivity     = viewClass.getActivity();
        openDb(this.myActivity, true);
    }

    /**
     * constructor with param isDbWrite
     * @param isDbWrite if the db is writable
     */
    public NewReceiptPresenter(boolean isDbWrite) {

        this.viewClass      = new NewReceiptFragment();
        this.myActivity     = viewClass.getActivity();
        openDb(this.myActivity, isDbWrite);
    }

    /**
     * constructor with the desired view class
     * @param viewClass the desired view class
     * @param isDbWrite if the db is writable
     */
    public NewReceiptPresenter(NewReceiptFragment viewClass, boolean isDbWrite) {
        this.viewClass      = viewClass;
        this.myActivity     = viewClass.getActivity();
        openDb(this.myActivity, isDbWrite);
    }

    /**
     * constructor with      the desired view class and model db manager
     * @param viewClass      the desired view class
     * @param modelDbManager the desired model db manager
     */
    public NewReceiptPresenter(NewReceiptFragment viewClass, ReceiptDbCrudManager modelDbManager) {
        this.viewClass      = viewClass;
        this.myActivity     = viewClass.getActivity();
        this.modelDbManager = modelDbManager;
    }

    // override
    @Override
    public void setViewClass(NewReceiptFragment viewClass) {

        this.viewClass = viewClass;
    }

    @Override
    public NewReceiptFragment getViewClass() {

        return this.viewClass;
    }

    @Override
    public void openDb(Context context, boolean isDbWrite) {

        // init db manager
        myLedgerDbManager = new MyLedgerDbManager();
        myLedgerDbManager.open(context, isDbWrite, this);
    }

    @Override
    public void closeDb() {

        // close db
        myLedgerDbManager.close();
    }

    @Override
    public void setModelDbManager(ReceiptDbCrudManager modelDbManager) {

        this.modelDbManager = modelDbManager;
    }

    @Override
    public ReceiptDbCrudManager getModelDbManager() {

        return this.modelDbManager;
    }

    @Override
    public void updateDbHelperManager() {

        // get helper
        this.myLedgerDbHelper   = myLedgerDbManager.getDbHelper();
        // get db
        this.db                 = myLedgerDbManager.getDb();
        // init crud manager
        this.modelDbManager     = new ReceiptDbCrudManager(getViewClass().getContext(), db);
    }

    @Override
    public void bind() {

        // get data from intent
        ReceiptObj myReceipt =
                (ReceiptObj) viewClass.getActivity().getIntent().getSerializableExtra(RECEIPT_OBJECT);
        // init. model object
        if (myReceipt == null) {

            myReceipt = ReceiptObj.newInstance();
        }
//        System.out.println("===\nreceipt object: " + myReceipt + "\n===");
        // bind data
        // order number
        viewClass.setOrderNumber(
                myReceipt.getOrderNumber()
        );
        // year
        viewClass.setYear(
                myReceipt.getYear()
        );
        // month
        viewClass.setMonth(
                myReceipt.getMonth()
        );
        // date
        viewClass.setDate(
                myReceipt.getDate()
        );
        // location
        viewClass.setLocation(
                myReceipt.getLocation()
        );
        // subtotal
        viewClass.setSubtotal(
                myReceipt.getSubtotal()
        );
        // tax
        viewClass.setTax(
                myReceipt.getTax()
        );
        // total
        viewClass.setTotal(
                myReceipt.getTotal()
        );

    }

    @Override
    public Cursor readAll() {

        return modelDbManager.readAll();
    }

    @Override
    public long insert() {

        // init.
        ContentValues cv = new ContentValues();
        // setup data
        cv.put(MyLedgerContract.Receipt.COLUMN_NAME_YEAR,        "1998");
        cv.put(MyLedgerContract.Receipt.COLUMN_NAME_MONTH,       "Dec");
        cv.put(MyLedgerContract.Receipt.COLUMN_NAME_DATE,        "24");
        cv.put(MyLedgerContract.Receipt.COLUMN_NAME_LOCATION,    "Multi-Verse");
        cv.put(MyLedgerContract.Receipt.COLUMN_NAME_SUBTOTAL,    100);
        cv.put(MyLedgerContract.Receipt.COLUMN_NAME_TAX,         5);
        cv.put(MyLedgerContract.Receipt.COLUMN_NAME_TOTAL,       105);

        return modelDbManager.insert(cv);
    }

    @Override
    public int update() {
        return 0;
    }

    @Override
    public int delete() {
        return 0;
    }
}
