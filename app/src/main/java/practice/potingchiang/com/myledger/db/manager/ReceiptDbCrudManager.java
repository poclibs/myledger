package practice.potingchiang.com.myledger.db.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import pandaz.library.potingchiang.com.db.interfaces.IDbCrudManager;
import practice.potingchiang.com.myledger.db.contract.MyLedgerContract.Receipt;

/**
 * This is the manager class to mange receipt CRUD
 * Created by potingchiang on 2017-06-24.
 */

public class ReceiptDbCrudManager implements IDbCrudManager {

    // context -> for future use
    private Context context;
    // db open helper
    private SQLiteDatabase db;
    // all columns
    private final static String[] tableColumns = {
            Receipt._ID,
            Receipt.COLUMN_NAME_YEAR,
            Receipt.COLUMN_NAME_MONTH,
            Receipt.COLUMN_NAME_DATE,
            Receipt.COLUMN_NAME_LOCATION,
            Receipt.COLUMN_NAME_SUBTOTAL,
            Receipt.COLUMN_NAME_TAX,
            Receipt.COLUMN_NAME_TOTAL
    };
    // sort order by _Id
    private final static String sortOrderByIdAsc =
            Receipt._ID + " ASC";

    /**
     * constructor with multiple parameters
     * @param context the context
     * @param db      the app database
     */
    public ReceiptDbCrudManager(Context context, SQLiteDatabase db) {
        this.context = context;
        this.db = db;
    }

    // getter & setter
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    public void setDb(SQLiteDatabase db) {
        this.db = db;
    }

    public static String[] getTableColumns() {
        return tableColumns;
    }

    public static String getSortOrderByIdAsc() {
        return sortOrderByIdAsc;
    }

    // basic CRUD
    @Override
    public Cursor readAll() {

        return db.query(
                Receipt.TABLE_NAME,
                tableColumns,
                null,
                null,
                null,
                null,
                sortOrderByIdAsc
        );
    }

    @Override
    public long insert(ContentValues cv) {

        if (!db.isReadOnly()) {

            // insert data and return the new inserted data id
            return db.insert(Receipt.TABLE_NAME, null, cv);
        }
        return -1;
    }

    @Override
    public int update(@NonNull ContentValues cv, String whereClause, String[] whereArgs) {

        if (!db.isReadOnly()) {

            return db.update(Receipt.TABLE_NAME, cv, whereClause, whereArgs);
        }
        return -1;
    }

    /**
     * delete function
     * @param whereClause To remove all rows and get a count pass "1" as the whereClause.
     * @param whereArgs is the string[] of placeholders(?) in @param whereClause
     * @return int rows affected
     */
    @Override
    public int delete(String whereClause, String[] whereArgs) {

        if (!db.isReadOnly()) {

            db.delete(Receipt.TABLE_NAME, whereClause, whereArgs);
        }
        return -1;
    }

    // more CRUD here....
}
