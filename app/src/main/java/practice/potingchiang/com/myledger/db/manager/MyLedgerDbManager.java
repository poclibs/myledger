package practice.potingchiang.com.myledger.db.manager;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import pandaz.library.potingchiang.com.db.interfaces.IDbManager;
import pandaz.library.potingchiang.com.mvp.presenter.IPresenter;
import practice.potingchiang.com.myledger.db.MyLedgerDbHelper;

/**
 * This class is to help open and close db helper and db
 * Created by potingchiang on 2017-07-04.
 */

public class MyLedgerDbManager implements IDbManager<MyLedgerDbHelper, SQLiteDatabase, IPresenter> {

    // context
    private Context context;
    // db open helper
    private MyLedgerDbHelper myLedgerDbHelper;
    // MyLedger Db
    private SQLiteDatabase  myDb;
    private Boolean isDbWrite;

    @Override
    public void open(Context context, Boolean isDbWrite, IPresenter presenter) {

        // update context
        this.context            = context;
        // init helper
        this.myLedgerDbHelper   = new MyLedgerDbHelper(context);
        // update is db writable
        this.isDbWrite          = isDbWrite;
        // get db in async task
        new GetDbAsync(this, presenter).execute();
//        if (isDbWrite) {
//
//            myDb = myLedgerDbHelper.getWritableDatabase();
//        }
//        else {
//
//            myDb = myLedgerDbHelper.getReadableDatabase();
//        }
    }

    @Override
    public void close() {

        // close helper
        this.myLedgerDbHelper.close();
        // close db
        this.myDb.close();
    }

    @Override
    public SQLiteDatabase getDb() {

        return myDb;
    }

    @Override
    public void setDb(SQLiteDatabase db) {

        this.myDb = db;
    }

    @Override
    public MyLedgerDbHelper getDbHelper() {

        return myLedgerDbHelper;
    }

    @Override
    public Boolean isDbWrite() {

        return this.isDbWrite;
    }

    @Override
    public Context getContext() {

        return this.context;
    }

    // get db in async task
    private class GetDbAsync extends AsyncTask<Void, Void, SQLiteDatabase> {

        // variables
        private MyLedgerDbManager myDbManagerInAsync;
        // progress dialog
        private ProgressDialog progressDialog;
        // callback
        private IPresenter presenter;

        // constructor
        public GetDbAsync(MyLedgerDbManager myLedgerDbManager, IPresenter presenter) {
            this.myDbManagerInAsync = myLedgerDbManager;
            this.presenter          = presenter;
        }

        // pre
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // only can do here
            // init progress dialog
            progressDialog = new ProgressDialog(myDbManagerInAsync.getContext());
            // setup progress dialog
            progressDialog.setTitle("Please wait");
            progressDialog.setMessage("Processing Database...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            // show progress dialog
            progressDialog.show();
        }

        // doing
        @Override
        protected SQLiteDatabase doInBackground(Void... params) {


            System.out.println("===\nIn Process" + "\n===");
            // init. db
            SQLiteDatabase db;
            // check if db is writable
            if (this.myDbManagerInAsync.isDbWrite()) {

                db = getDbHelper().getWritableDatabase();
            }
            else {

                db = getDbHelper().getReadableDatabase();
            }

            return db;
        }
        // post
        @Override
        protected void onPostExecute(SQLiteDatabase sqLiteDatabase) {
            super.onPostExecute(sqLiteDatabase);

            // update db
            this.myDbManagerInAsync.setDb(sqLiteDatabase);
            // update info
            this.presenter.updateDbHelperManager();
            // dismiss progress dialog
            progressDialog.dismiss();
        }

    }

}
