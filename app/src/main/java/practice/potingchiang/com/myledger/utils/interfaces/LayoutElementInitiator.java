package practice.potingchiang.com.myledger.utils.interfaces;

/**
 * This is the interface for layout elements initiation
 * Created by potingchiang on 2017-07-02.
 */

public interface LayoutElementInitiator {

    void initLayoutElements();
    void initLayoutElementsListener();
}
