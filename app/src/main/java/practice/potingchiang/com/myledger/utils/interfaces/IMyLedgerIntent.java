package practice.potingchiang.com.myledger.utils.interfaces;

/**
 * This interface for the app intent manager
 * Created by potingchiang on 2017-07-04.
 */

public interface IMyLedgerIntent<M> {

    // landing ac
    void toLandingAc();
    // add new receipt
    void toNewReceiptAc(M data);
}
