package practice.potingchiang.com.myledger.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import pandaz.library.potingchiang.com.db.PandaZDbHelper;
import practice.potingchiang.com.myledger.db.contract.MyLedgerContract.Receipt;

/**
 * This is db my ledger db helper to help create db
 * Created by potingchiang on 2017-06-20.
 */

public class MyLedgerDbHelper extends PandaZDbHelper {

    // basic variables
    // db version
    public static final int     DATABASE_VERSION    = 1;
    // db name (.db)
    public static final String  DATABASE_NAME       = "MyLedger.db";
    // db syntax
    // receipt
    // create
    private static final String SQL_CREATE_RECEIPT =
            CREATE_TABLE_NOT_EXIST + Receipt.TABLE_NAME +
                    OPENING +
                    Receipt._ID + PRIMARY_KEY_INTEGER_AUTO_STATEMENT    + COMMA_SEP +
                    Receipt.COLUMN_NAME_YEAR        + SPACE + DataType.TEXT     + COMMA_SEP +
                    Receipt.COLUMN_NAME_MONTH       + SPACE + DataType.TEXT     + COMMA_SEP +
                    Receipt.COLUMN_NAME_DATE        + SPACE + DataType.TEXT     + COMMA_SEP +
                    Receipt.COLUMN_NAME_LOCATION    + SPACE + DataType.TEXT     + COMMA_SEP +
                    Receipt.COLUMN_NAME_SUBTOTAL    + SPACE + DataType.REAL     + SPACE     + NOT_NULL  + COMMA_SEP +
                    Receipt.COLUMN_NAME_TAX         + SPACE + DataType.REAL     + SPACE     + NOT_NULL  + COMMA_SEP +
                    Receipt.COLUMN_NAME_TOTAL       + SPACE + DataType.REAL     + SPACE     + NOT_NULL  +
                    CLOSING;
    // drop
    private static final String SQL_DROP_RECEIPT =
            DROP_TABLE_EXIST + Receipt.TABLE_NAME;

    // constructor
    public MyLedgerDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // necessary override methods (for now)
    // create
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQL_CREATE_RECEIPT);
    }
    // update
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // drop old
        db.execSQL(SQL_DROP_RECEIPT);
        // recreate
        onCreate(db);
    }
}
