package practice.potingchiang.com.myledger.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import pandaz.library.potingchiang.com.time.SysDateTimeHandler;
import practice.potingchiang.com.myledger.R;
import practice.potingchiang.com.myledger.presenters.NewReceiptPresenter;
import practice.potingchiang.com.myledger.utils.MyLedgerIntentManager;
import practice.potingchiang.com.myledger.utils.interfaces.LayoutElementInitiator;

/**
 * This is the add new receipt fragment
 * A placeholder fragment containing a simple view.
 */
public class NewReceiptFragment extends Fragment
        implements LayoutElementInitiator, NewReceiptPresenter.IAddNewReceiptPresenter {

    // fragment view
    private View myView;
    // layout elements
    // <!--year    text-->
    private TextView txtYear;
    // <!--month   text-->
    private TextView txtMonth;
    // <!--date    text-->
    private TextView txtDate;
    // <!--order#  text-->
    private TextView txtOrderNumber;
    // <!--location    edit text-->
    private EditText editTxtLocation;
    // <!--subtotal    edit text-->
    private EditText editTxtSubtotal;
    // <!--tax         edit text-->
    private EditText editTxtTax;
    // <!--total       edit text-->
    private EditText editTxtTotal;
    // <!--cancel-->
    private Button   btnCancelNewReceipt;
    // <!--okay-->
    private Button   btnOkNewReceipt;
    // presenter
    private NewReceiptPresenter myPresenter;
    // layout elements
    public NewReceiptFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // inflate with fragment
        this.myView = inflater.inflate(R.layout.fragment_new_receipt, container, false);
        // int layout elements and listener
        initLayoutElements();
        // bind with presenter
        bindWithPresenter();

        return this.myView;
    }

    @Override
    public void initLayoutElements() {

        // <!--year    text-->
        txtYear             = (TextView) this.myView.findViewById(R.id.txtYear);
        // <!--month   text-->
        txtMonth            = (TextView) this.myView.findViewById(R.id.txtMonth);
        // <!--date    text-->
        txtDate             = (TextView) this.myView.findViewById(R.id.txtDate);
        // <!--order#  text-->
        txtOrderNumber      = (TextView) this.myView.findViewById(R.id.txtOrderNumber);
        // <!--location    edit text-->
        editTxtLocation     = (EditText) this.myView.findViewById(R.id.editTxtLocation);
        // <!--subtotal    edit text-->
        editTxtSubtotal     = (EditText) this.myView.findViewById(R.id.editTxtSubtotal);
        // <!--tax         edit text-->
        editTxtTax          = (EditText) this.myView.findViewById(R.id.editTxtTax);
        // <!--total       edit text-->
        editTxtTotal        = (EditText) this.myView.findViewById(R.id.editTxtTotal);
        // cancel
        btnCancelNewReceipt = (Button) this.myView.findViewById(R.id.btnCancelNewReceipt);
        // ok
        btnOkNewReceipt     = (Button) this.myView.findViewById(R.id.btnOkNewReceipt);

        // click listener
        initLayoutElementsListener();
    }

    @Override
    public void initLayoutElementsListener() {

        // on click
        View.OnClickListener buttonOnClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (v.getId()) {

                    // cancel
                    case R.id.btnCancelNewReceipt: {

                        // to landing ac
                        new MyLedgerIntentManager(getActivity()).toLandingAc();
                        // close db
                        myPresenter.closeDb();
                        // destroy the current ac/fragment
                        getActivity().finish();

                        break;
                    }
                    // ok
                    case R.id.btnOkNewReceipt: {

                        // insert data
                        myPresenter.insert();
                        // toast
                        Toast.makeText(getContext(), "Insert Successfully", Toast.LENGTH_SHORT).show();
                        // check db size
                        System.out.println("===\nMy DB size: " + myPresenter.readAll().getCount() + "\n===");
                        // to landing ac
                        new MyLedgerIntentManager(getActivity()).toLandingAc();
                        // close db
                        myPresenter.closeDb();
                        // destroy the current ac/fragment
                        getActivity().finish();
                        break;
                    }
                    // others
                    default: {

                        break;
                    }
                }
            }
        };

        // setup button on click
        btnCancelNewReceipt.setOnClickListener(buttonOnClick);
        btnOkNewReceipt.setOnClickListener(buttonOnClick);
    }

    @Override
    public void bindWithPresenter() {

        myPresenter = new NewReceiptPresenter(this, true);
        myPresenter.bind();
    }

    @Override
    public Integer getOrderNumber() {

        return txtOrderNumber.getText().toString().isEmpty() ?
                -1 : Integer.parseInt(txtOrderNumber.getText().toString());
    }

    @Override
    public void setOrderNumber(Integer orderNumber) {

        txtOrderNumber.setText(String.format(Locale.CANADA, "%d", orderNumber));
        if (orderNumber == null || orderNumber == 0) {

            txtOrderNumber.setVisibility(View.GONE);
        }
        else {

            txtOrderNumber.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public String getYear() {

        return txtYear.getText().toString();
    }

    @Override
    public void setYear(String year) {

        txtYear.setText(year);
        if (year == null || year.isEmpty()) {

            txtYear.setText(
                    new SysDateTimeHandler().getYear()
            );
        }
    }

    @Override
    public String getMonth() {

        return txtMonth.getText().toString();
    }

    @Override
    public void setMonth(String month) {

        txtMonth.setText(month);
        if (month == null || month.isEmpty()) {

            txtMonth.setText(
                    new SysDateTimeHandler().getMonth()
            );

//            System.out.println("===\nmonth: " + new SysDateTimeHandler().getMonth() + "\n===");
        }
    }

    @Override
    public String getDate() {

        return txtDate.getText().toString();
    }

    @Override
    public void setDate(String date) {

        txtDate.setText(date);
        if (date == null || date.isEmpty()) {

            txtDate.setText(
                    new SysDateTimeHandler().getDateDD()
            );

//            System.out.println("===\nmonth: " + new SysDateTimeHandler().getDateDD() + "\n===");
        }
    }

    @Override
    public String getLocation() {

        return editTxtLocation.getText().toString();
    }

    @Override
    public void setLocation(String location) {

        editTxtLocation.setText(location);
    }

    @Override
    public float getSubtotal() {

        return editTxtSubtotal.getText().toString().isEmpty() ?
                0 : Float.parseFloat(editTxtSubtotal.getText().toString());
    }

    @Override
    public void setSubtotal(Float subtotal) {

        if (subtotal != null)
            editTxtSubtotal.setText(String.format(Locale.CANADA, "%.2f", subtotal));
    }

    @Override
    public float getTax() {

        return editTxtTax.getText().toString().isEmpty() ?
                0 : Float.parseFloat(editTxtTax.getText().toString());
    }

    @Override
    public void setTax(Float tax) {

        if (tax != null)
            editTxtTax.setText(String.format(Locale.CANADA, "%.2f", tax));
    }

    @Override
    public float getTotal() {

        return editTxtTotal.getText().toString().isEmpty() ?
                0 : Float.parseFloat(editTxtTotal.getText().toString());
    }

    @Override
    public void setTotal(Float total) {

        if (total != null)
            editTxtTotal.setText(String.format(Locale.CANADA, "%.2f", total));
    }
}
