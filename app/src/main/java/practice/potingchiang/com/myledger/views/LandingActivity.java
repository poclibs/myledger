package practice.potingchiang.com.myledger.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import practice.potingchiang.com.myledger.R;
import practice.potingchiang.com.myledger.utils.MyLedgerIntentManager;

public class LandingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        // tool bar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    // on new receipt button click
    public void onNewReceiptClick(View view) {

        // go to add new receipt ac
        new MyLedgerIntentManager(LandingActivity.this).toNewReceiptAc(null);
    }
    // on view receipt button click
    public void onViewReceiptClick(View view) {

    }
}
