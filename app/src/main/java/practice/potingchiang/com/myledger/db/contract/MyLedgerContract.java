package practice.potingchiang.com.myledger.db.contract;

import android.provider.BaseColumns;

/**
 * This class is the contract class for ledger db schema
 * Created by potingchiang on 2017-06-20.
 */

public final class MyLedgerContract {

    // private constructor
    private MyLedgerContract(){}

    // tables
    // receipt
    public static class Receipt implements BaseColumns {

        // table name
        public static final String TABLE_NAME               = "receipt";
        // columns
        // year
        public static final String COLUMN_NAME_YEAR         = "year";
        // month
        public static final String COLUMN_NAME_MONTH        = "month";
        // date
        public static final String COLUMN_NAME_DATE         = "date";
        // location
        public static final String COLUMN_NAME_LOCATION    = "location";
        // subtotal
        public static final String COLUMN_NAME_SUBTOTAL     = "subtotal";
        // tax/gst
        public static final String COLUMN_NAME_TAX          = "tax";
        // total
        public static final String COLUMN_NAME_TOTAL        = "total";


    }
}
