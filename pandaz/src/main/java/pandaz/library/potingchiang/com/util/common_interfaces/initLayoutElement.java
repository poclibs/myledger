package pandaz.library.potingchiang.com.util.common_interfaces;

/**
 * This is interface is for layout elements to initiate
 * Created by potingchiang on 2017-06-20.
 */

public interface initLayoutElement {

    void initElements();
    void initListeners();
}
