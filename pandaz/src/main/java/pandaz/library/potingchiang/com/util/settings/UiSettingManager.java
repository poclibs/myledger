package pandaz.library.potingchiang.com.util.settings;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;

/**
 * This class is to setup ui
 * Created by potingchiang on 2016-07-18.
 */
public class UiSettingManager {

    //basic variables
    private Context mContext;

    //constructor
    public UiSettingManager(Context mContext) {
        this.mContext = mContext;
    }

    //getter & setter

    //other methods
    //get custom fonts
    public Typeface getCustomTypeface(String ttf_Font) {

        //declare asset manager
        AssetManager mAssetManager = null;
        //check if context is null
        try {

            //init asset manager
            mAssetManager = mContext.getAssets();

        } catch (Exception e) {

            //error messages
            System.out.println("===\n" + "Context is null!" + "\n===");
            System.out.println("===\n" + e.getMessage() + "\n===");
        }

        //init font path
        String font_Path = "fonts/" + ttf_Font;

        //return result
        return Typeface.createFromAsset(mAssetManager, font_Path);
    }
}
