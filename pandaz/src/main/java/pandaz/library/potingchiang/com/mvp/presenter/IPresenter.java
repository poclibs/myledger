package pandaz.library.potingchiang.com.mvp.presenter;

import android.content.Context;
import android.database.Cursor;

/**
 * This is the basic presenter for others to use/extend
 * Created by potingchiang on 2017-07-02.
 *
 * @param <V> is the desired class(activity or fragment) for the view to passed in
 * @param <M> is the desired db table handler with the certain model to passed in
 */
public interface IPresenter<V, M> {

    // set V
    void setViewClass(V viewClass);
    // get V
    V getViewClass();
    // open helper & db
    void openDb(Context context, boolean isDbWrite);
    void closeDb();
    // close helper & db
    // set M
    void setModelDbManager(M modelDbManager);
    // get M
    M getModelDbManager();
    // update info
    void updateDbHelperManager();
    // bind V with M
    void bind();
    // read
    Cursor readAll();
    // insert
    long insert();
    // update
    int update();
    // delete
    int delete();
}
