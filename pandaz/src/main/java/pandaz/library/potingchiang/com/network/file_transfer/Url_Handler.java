package pandaz.library.potingchiang.com.network.file_transfer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * This class is to handle all url realted operation
 * Created by potingchiang on 2016-09-26.
 */

public class Url_Handler {

    //basic url
//    private Context context;
    private String url;

    //constructor
    public Url_Handler(String url) {
        this.url = url;
    }

    //method
    //get image from url - return drawable
    public Drawable getImgFromUrl_Drawable(String img_Name) {

        //get input stream
        try {


            //check message
            System.out.println("===\nBefore get img from url " + "\n===");
            //get input stream
            InputStream is = (InputStream) new URL(url).getContent();
            //check message
            System.out.println("===\ninput stream: " + is.toString() + "\n===");

            return Drawable.createFromStream(is, img_Name);

        } catch (IOException e) {

            //message
            e.printStackTrace();
            System.out.println("===\nerror message: " + e.getMessage() + "\n===");

            return null;
        }

    }

    //get image from url - return drawable
    public byte[] getImgFromUrl_ByteArray(Bitmap.CompressFormat format, int quality) {

        //get input stream
        try {


            //check message
            System.out.println("===\nBefore get img from url " + "\n===");
            //get input stream
            InputStream is = (InputStream) new URL(url).getContent();
            //check message
            System.out.println("===\ninput stream: " + is.toString() + "\n===");
            //transfer to bitmap
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            //compress bit with in byte array output stream
            //init byte array out put stream
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            //compress bitmap into output stream
            bitmap.compress(format, quality, outputStream);

            return outputStream.toByteArray();

        } catch (IOException e) {

            //message
            e.printStackTrace();
            System.out.println("===\nerror message: " + e.getMessage() + "\n===");

            return null;
        }

    }
}
