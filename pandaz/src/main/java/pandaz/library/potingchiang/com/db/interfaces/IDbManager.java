package pandaz.library.potingchiang.com.db.interfaces;

import android.content.Context;

/**
 * The interface to interact with db opening and closing
 * Created by potingchiang on 2017-07-04.
 */

public interface IDbManager<H, D, P> {

    void open(Context context, Boolean isDbWrite, P presenter);
    void close();
    D getDb();
    void setDb(D db);
    H getDbHelper();
    Boolean isDbWrite();
    Context getContext();

}
