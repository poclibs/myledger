package pandaz.library.potingchiang.com.time;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * This class is to generate system date
 * Created by potingchiang on 2016-02-02.
 */
public class SysDateTimeHandler {

    private SimpleDateFormat sysDate;
    private SimpleDateFormat sysDateTime;
    private Locale locale;
    private String date_Format;
    private String dateTime_Format;

    public SysDateTimeHandler() {

        this.locale = Locale.CANADA;
    }
    public SysDateTimeHandler(Locale locale) {

        this.locale = locale;
    }
    public String getYear() {

        date_Format = "yyyy";
         return new SimpleDateFormat(date_Format, locale).format(new Date());
    }
    public String getMonth() {

        date_Format = "MMMM";
        return new SimpleDateFormat(date_Format, locale).format(new Date());
    }
    public String getDateDD() {

        date_Format = "dd";
        return new SimpleDateFormat(date_Format, locale).format(new Date());
    }
    public String getSysDateMMDDYYYY() {

        date_Format = "MMM-dd-yyyy";
        sysDate = new SimpleDateFormat(date_Format, locale);

        return sysDate.format(new Date());
    }
    public String getSysDateTime() {

        dateTime_Format = "MMM-dd-yyyy HH:mm";
        sysDateTime = new SimpleDateFormat(dateTime_Format, locale);

        return sysDateTime.format(new Date());
    }
    public String getSysTimeHour() {

        dateTime_Format = "hh";
        sysDateTime = new SimpleDateFormat(dateTime_Format, locale);

        return sysDateTime.format(new Date());
    }
    public String getSysTimeMin() {

        dateTime_Format = "mm";
        sysDateTime = new SimpleDateFormat(dateTime_Format, locale);

        return sysDateTime.format(new Date());
    }
    public String getSysTimeSec() {

        dateTime_Format = "ss";
        sysDateTime = new SimpleDateFormat(dateTime_Format, locale);

        return sysDateTime.format(new Date());
    }
    public String getSysAmPm() {

        dateTime_Format = "a";
        sysDateTime = new SimpleDateFormat(dateTime_Format, locale);

        return sysDateTime.format(new Date());
    }
}
