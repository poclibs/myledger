package pandaz.library.potingchiang.com.network.file_transfer;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;

/**
 * This class is for restful GET image basic setup
 * Created by potingchiang on 2016-07-26.
 */
public class GET_IMG_FromURL_ByteArray extends AsyncTask<String, Void, byte[]> {

    //basic variables
    private Context context;
    private ProgressDialog progressDialog;
    private Bitmap.CompressFormat format;
    private int quality;

    //constructor
    public GET_IMG_FromURL_ByteArray(Context context, Bitmap.CompressFormat format, int quality) {
        this.context = context;
        this.format = format;
        this.quality = quality;
    }

    //override method
    @Override
    protected void onPreExecute() {
//        super.onPreExecute();

//        //init progress dialog
//        progressDialog = ProgressDialog.show(
//                context,
//                "Please wait", "Processing...",
//                true,
//                true
//        );
    }

    @Override
    protected byte[] doInBackground(String... params) {

        //setup url
        String url = params[0];
        //init url handler
        //init talk to server with GET
        Url_Handler url_handler = new Url_Handler(url);
        //return result
        return  url_handler.getImgFromUrl_ByteArray(format, quality);
    }

    //getter & setter
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }
}
