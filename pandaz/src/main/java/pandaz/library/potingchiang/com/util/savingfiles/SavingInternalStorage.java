package pandaz.library.potingchiang.com.util.savingfiles;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import pandaz.library.potingchiang.com.time.SysDateTimeHandler;
import pandaz.library.potingchiang.com.util.converter.StreamStringConverter;

/**
 *  This is for saving files in the Android internal storage
 * Created by potingchiang on 2017-05-20.
 */

public class SavingInternalStorage implements ISavingFiles {

    // variables
    private Context myContext;
    private String filename;
    private FileOutputStream fos;
    private FileInputStream fis;
    // constructor
    public SavingInternalStorage(Context myContext, String filename) {

        this.myContext = myContext;
        this.filename = filename;
    }

    @Override
    public void openFileWrite(String content) {

//        SysDateTimeHandler dateTimeHandler = new SysDateTimeHandler();
        try {

            // check file if it exists
            File myFile = new File(myContext.getFilesDir(), filename);
            // message
            System.out.println("===\nmy file - write: " + myFile.toString() + "\n===");
            // init mode
            int mode;
            if (myFile.exists()) {

                mode = Context.MODE_APPEND;
            }
            else {

                mode = Context.MODE_PRIVATE;
            }
            // init file output stream
            fos = myContext.openFileOutput(filename, mode);
            // add date time to content
            content = "\n" + new SysDateTimeHandler().getSysDateTime() + "\n" + content + "\n";
            // write data/content into it
            fos.write(content.getBytes());
            // close stream
            fos.close();

        } catch (FileNotFoundException e) {

            e.printStackTrace();
            // message
            System.out.println("===\nFileNotFoundException: " + e.getMessage() + "\n===");
        } catch (IOException e) {

            e.printStackTrace();
            // message
            System.out.println("===\nIOException: " + e.getMessage() + "\n===");
        }
    }

    @Override
    public String openFileRead() {

        String result = "no data available";
        try {

            // get input stream
            fis = myContext.openFileInput(filename);
            // message
            System.out.println("===\nmy file - read: " + fis.toString()+ "\n===");
            // read data using buffer reader
            if (fis != null) {

                result = new StreamStringConverter().convertInputStreamToString(fis);
            }

        } catch (FileNotFoundException e) {

            e.printStackTrace();
            // message
            System.out.println("===\nFileNotFoundException: " + e.getMessage() + "\n===");
        } catch (IOException e) {

            e.printStackTrace();
            // message
            System.out.println("===\nIOException: " + e.getMessage() + "\n===");
        }

        return result;
    }


}
