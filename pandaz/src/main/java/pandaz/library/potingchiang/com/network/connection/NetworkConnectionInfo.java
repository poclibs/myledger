package pandaz.library.potingchiang.com.network.connection;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * This class is to check if network is connected
 * Created by potingchiang on 2016-06-22.
 */
public class NetworkConnectionInfo {

    private Context context;
    private boolean isConnected = false;
    private NetworkInfo networkInfo;

    public NetworkConnectionInfo(Context context) {
        this.context = context;
    }

    public boolean isConnected() {

        //init connection manager/mgr
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(
                Activity.CONNECTIVITY_SERVICE
        );

        //get network info
        networkInfo = connMgr.getActiveNetworkInfo();

        //check network connection
        if (networkInfo != null && networkInfo.isConnected()) {

            isConnected = true;
        }
        else {

            isConnected = false;
        }

        return isConnected;
    }

    //getter & setter

    public NetworkInfo getNetworkInfo() {
        return networkInfo;
    }
}
