package pandaz.library.potingchiang.com.util.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 *  handle default alert dialog
 * Created by potingchiang on 2017-01-07.
 */

public abstract class MyAlertDialog_Def {

    //basic variables
    private Context context;
    private String title;
    private String message;
    private String positiveBtn_title;
    private String negativeBtn_title;

    //constructor
    public MyAlertDialog_Def(Context context, String title, String message) {
        this.context = context;
        this.title = title;
        this.message = message;
    }

    public MyAlertDialog_Def(Context context, String title, String message, String positiveBtn_title,
                             String negativeBtn_title) {
        this.context = context;
        this.title = title;
        this.message = message;
        this.positiveBtn_title = positiveBtn_title;
        this.negativeBtn_title = negativeBtn_title;
    }

    //on create
    public AlertDialog onCreateDialog() {

        //init. dialog
        AlertDialog.Builder myBuilder = new AlertDialog.Builder(context);
        //setup dialog info
        myBuilder.setTitle(title);
        myBuilder.setMessage(message);
        if (!getPositiveBtn_title().isEmpty()) {

            myBuilder.setPositiveButton(getPositiveBtn_title(), dialogOnClickListener());
        }
        if (!getNegativeBtn_title().isEmpty()) {

            myBuilder.setNegativeButton(getNegativeBtn_title(), dialogOnClickListener());
        }

        return myBuilder.create();
    }

    //abstract methods
    public abstract DialogInterface.OnClickListener dialogOnClickListener();

    //getter & setter
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPositiveBtn_title() {
        return positiveBtn_title;
    }

    public void setPositiveBtn_title(String positiveBtn_title) {
        this.positiveBtn_title = positiveBtn_title;
    }

    public String getNegativeBtn_title() {
        return negativeBtn_title;
    }

    public void setNegativeBtn_title(String negativeBtn_title) {
        this.negativeBtn_title = negativeBtn_title;
    }


}
