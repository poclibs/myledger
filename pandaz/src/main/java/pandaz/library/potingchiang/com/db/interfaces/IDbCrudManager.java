package pandaz.library.potingchiang.com.db.interfaces;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * This is for basic CRUD manager interface
 * Created by potingchiang on 2017-06-24.
 */

public interface IDbCrudManager {

    // basic
    // read/query all
    Cursor readAll();

    // insert
    long insert(ContentValues cv);

    // update
    /**
     * update
     * @param cv the new ContentValues used to update the old ones
     * @param whereClause ex: whereClause = "my title like/= ?"
     * @param whereArgs is/are the value(s) for the "?"
     * @return the row(s) affected
     */
    int update(ContentValues cv, String whereClause, String[] whereArgs);

    // delete
    int delete(String whereClause, String[] whereArgs);

}
