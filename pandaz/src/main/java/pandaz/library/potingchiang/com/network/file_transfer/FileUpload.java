package pandaz.library.potingchiang.com.network.file_transfer;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * This class is to upload files to remote server
 * Created by potingchiang on 2016-06-19.
 */
public class FileUpload {

    //declaration
    private String mResponseMsg;
    private boolean isSuccess;
    private OnFileUploadListener mOnFileUploadListener;

    //constructor

    public FileUpload() {

        //init. response msg
        mResponseMsg = "";
        //init. isSuccess
        isSuccess = false;
    }

    //inner interface
    public interface OnFileUploadListener {

        void onFileUploadSuccess(String msg);
        void onFileUploadFail(String msg);
    }

    //getter & setter
    public OnFileUploadListener getmOnFileUploadListener() {
        return mOnFileUploadListener;
    }
    public void setmOnFileUploadListener(OnFileUploadListener mOnFileUploadListener) {
        this.mOnFileUploadListener = mOnFileUploadListener;
    }

    //other methods
    public boolean isSuccess() {

        return  isSuccess;
    }
    //do upload file
    public void doFileUpload(String path) {

        //declaration
        //http url connection
        HttpURLConnection connection= null;
        //data input stream
        DataOutputStream dataOutputStream = null;
        //data input stream
        DataInputStream dataInputStream = null;
        //data buffer reader => for input stream reader
        BufferedReader in = null;
        //file name/path
        String existingFileName = path;
        //each line end/separator
        String lineEnd = "\r\n";
        //hyphen
        String twoHyphens = "--";
        //boundary ???
        String boundary = "*****";
        //size
        int bytesRead, bytesAvailable, bufferSize;
        //init byte array
        byte[] buffer;
        //buffer max size
        int maxBufferSize = 1 * 1024 * 1024;
        //url
        String urlString = "http://192.168.0.52:8886/upload.php";

        try {

//            File mFile = new File(existingFileName);
            //client request
            FileInputStream fileInputStream = new FileInputStream(new File(existingFileName));
            //open an URL connection to servlet
            URL url = new URL(urlString);
            //ope http connection
            connection = (HttpURLConnection) url.openConnection();
            //setup attr. for connection
            //allow input
            connection.setDoInput(true);
            //allow out put
            connection.setDoOutput(true);
            //no cashed
            connection.setUseCaches(false);
            //setup action -> see doc. or source code
            connection.setRequestMethod("POST");
            //??? setup request property
            //setup request request header
            connection.setRequestProperty("Connection", "Keep-Alive");
//            connection.setRequestProperty("Content-Type", "Application/JSon");
//            connection.setRequestProperty("Accept", "Application/JSon");
            connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
//            connection.setRequestProperty("Content-Type", "image/jpg;boundary=" + boundary);
            //handle data output
            //init. data output stream
            dataOutputStream = new DataOutputStream(connection.getOutputStream());
            //??? get output stream "--****\r\n" ???
            //setup resources by the request
            dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
            dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" +
                    existingFileName + "\"" + lineEnd );
            dataOutputStream.writeBytes(lineEnd);
            //create a buffer of maxi size
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];
            //read file and write it into form...
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            //check stream data
            while (bytesRead > 0) {

                //write data into buffer
                dataOutputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                //read next bytes in the input stream if available
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            //send multipart form data necessary after file data...
            dataOutputStream.writeBytes(lineEnd);
            dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            //close streams
            fileInputStream.close();
            dataOutputStream.flush();
            dataOutputStream.close();
            //setup is success
            isSuccess = true;

        } catch (FileNotFoundException e) {
            //setup isSuccess & error message
            isSuccess = false;
            mResponseMsg = e.getMessage();
        } catch (MalformedURLException e) {
            //setup isSuccess & error message
            isSuccess = false;
            mResponseMsg = e.getMessage();

        } catch (IOException e) {
            //setup isSuccess & error message
            isSuccess = false;
            mResponseMsg = e.getMessage();
        }

        //handle data input
        try {

            //init. input stream
//            dataInputStream = new DataInputStream(connection.getInputStream());
            String str;
            //read data from input stream
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((str = in.readLine()) != null) {

                mResponseMsg = str;
            }
            //close buffer reader
            in.close();

//            //dataInputStream.readLine() is deprecated in API 1
//            while ((str = dataInputStream.readLine()) != null) {
//
//                mResponseMsg = str;
//            }
//            //close input stream
//            dataInputStream.close();

        } catch (IOException e) {

            //setup failure isSuccess and error messages
            isSuccess = false;
            mResponseMsg = e.getMessage();
        }

        if (mOnFileUploadListener != null) {

            if (isSuccess) {

                mOnFileUploadListener.onFileUploadSuccess(mResponseMsg);
            }
            else {

                mOnFileUploadListener.onFileUploadFail(mResponseMsg);
            }
        }
    }
}
