package pandaz.library.potingchiang.com.network.rest;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import pandaz.library.potingchiang.com.network.connection.TalkToServer;

/** This class is for restful POST basic setup
 * Created by potingchiang on 2016-07-26.
 */
public abstract class RESTfulPOST extends AsyncTask<String, Void, String> {

    //basic variables
    private Context context;
    private JSONObject request_JSon;
    private ProgressDialog progressDialog;
    private String httpStatusCode;
    private String connErrorMessage;
    private boolean isWriteLogFiles;
    // init file name
    public final static String FILENAME_POSTLOG = "pos_output_log";

    //constructor
    public RESTfulPOST(Context context, boolean isWriteLogFiles) {
        this.context = context;
        this.isWriteLogFiles = isWriteLogFiles;
    }

    @Override
    protected void onPreExecute() {

        //init progress dialog
        progressDialog = new ProgressDialog(context);

        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Processing...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);

        progressDialog.show();

        //init request json
        this.request_JSon = getRequest_JSon();

        //dump request data
        System.out.println("\n===\nData Json: \n" + request_JSon.toString() + "\n===");
        // write log files into internal storage
//        if (isWriteLogFiles) {
//
//            // write or append file into the device
//            new SavingInternalStorage(context, FILENAME_POSTLOG).openFileWrite(request_JSon.toString());
//        }
    }

    @Override
    protected String doInBackground(String... params) {

        //get url string
        String url = params[0];
        //do sync to server db
        //do request and response
        //init post to server
        TalkToServer doPost = new TalkToServer(url, "POST");
        //do post, get response and  get result
        String result_res = doPost.doPostAndGetResponse(request_JSon);
        //setup status code
        httpStatusCode = doPost.getConnStatusCode();
        connErrorMessage = doPost.getConnErrorMessage();
        //error message
        System.out.println("===\nConnection error message: " + connErrorMessage + "\n===");
        //return result
        return result_res;
    }

    //override method
    //get request json
    public abstract JSONObject getRequest_JSon();

    //getter & setter
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

//    public JSONObject getRequest_JSon() {
//        return request_JSon;
//    }
//
//    public void setRequest_JSon(JSONObject request_JSon) {
//        this.request_JSon = request_JSon;
//    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

    public String getHttpStatusCode() {
        return httpStatusCode;
    }

    //get response error message
    public String getConnectionErrorMessage() {

        return connErrorMessage;
    }

    public void setHttpStatusCode(String httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }
}
