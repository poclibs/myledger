package pandaz.library.potingchiang.com.network.connection;

import android.util.Base64;

/**
 * This class is to do restful authentication
 * Created by potingchiang on 2016-11-09.
 */

public class Authorization {

    //basic variables
    private String userName;
    private String password;
    private String authorization;
    //constructor
    public Authorization() {
    }
    public Authorization(String userName, String password) {
        this.userName = userName;
        this.password = password;
        authorization = userName + ":" + password;
    }

    //methods
    //get basic base64 encode
    public String getEncodedAuthorization_Base64() {

        //return encoded authentication
        return Base64.encodeToString(this.authorization.getBytes(), Base64.DEFAULT);
    }
    //getter & setter
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
