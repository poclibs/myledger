package pandaz.library.potingchiang.com.util.dialog.pos_alert_dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import pandaz.library.potingchiang.com.util.dialog.MyAlertDialog_Def;

/**
 * Handle print alert dialog
 * Created by potingchiang on 2017-01-07.
 */

public class Print_AlertDialog extends MyAlertDialog_Def {

    private AlertDialog alertDialog;
    public Print_AlertDialog(Context context, String title, String message) {
        super(context, title, message);

        setNegativeBtn_title("No");
        setPositiveBtn_title("Yes");

        alertDialog = onCreateDialog();

    }

    @Override
    public DialogInterface.OnClickListener dialogOnClickListener() {

        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

//                int positive = alertDialog.BUTTON_POSITIVE;
//                int negative = alertDialog.BUTTON_NEGATIVE;
                switch (which) {

                    //alertDialog.BUTTON_POSITIVE = -1
                    case -1: {


                        break;
                    }
                    //alertDialog.BUTTON_NEGATIVE = -2
                    case -2: {

                        break;
                    }

                }

            }
        };

        return onClickListener;
    }
}
