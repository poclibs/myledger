package pandaz.library.potingchiang.com.util.converter;

import android.support.annotation.NonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 *  This class is to convert stream to string back and forth
 * Created by potingchiang on 2017-05-20.
 */

public class StreamStringConverter {


    public String convertInputStreamToString(@NonNull InputStream fis) throws IOException {
        // init string builder
        StringBuilder result = new StringBuilder();
        // init each line string
        String line;
        // init buffer reader
        BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
        // if fis is not null
        while ((line = reader.readLine()) != null) {

            result.append(line).append("\n");
        }
        // close
        reader.close();
        // return result
        return result.toString();
    }
}
