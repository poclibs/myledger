package pandaz.library.potingchiang.com.network.rest;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import pandaz.library.potingchiang.com.network.connection.TalkToServer;


/**
 * This class is for restful GET basic setup
 * Created by potingchiang on 2016-07-26.
 */
public class RESTfulGET extends AsyncTask<String, Void, String> {

    //basic variables
    private Context context;
    private ProgressDialog progressDialog;
    private String httpStatusCode;

    //constructor
    public RESTfulGET(Context context) {
        this.context = context;
    }

    //override method
    @Override
    protected void onPreExecute() {
//        super.onPreExecute();

        //init progress dialog
        progressDialog = new ProgressDialog(context);

        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Processing...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);

        if (!progressDialog.isShowing()) {

            progressDialog.show();
        }

    }

    @Override
    protected String doInBackground(String... params) {

        //setup url
        String url = params[0];
        //init talk to server with GET
        TalkToServer doGET = new TalkToServer(url, "GET");
        //get result from server
        String result = doGET.doGetAndResponse();
        //get status code
        httpStatusCode = doGET.getConnStatusCode();
        //return result
        return result;
    }

    //getter & setter
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

    public String getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(String httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }
}
