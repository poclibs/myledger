package pandaz.library.potingchiang.com.network.connection;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class CheckPingHttpConnection {

    //declaration
    //url
    private String url;
    //http connection
    private HttpURLConnection conn;
    private DataOutputStream dataOutputStream;
    private BufferedReader bufferedReader;
    private String request_Method;
    //message
    String message;

    //constructor
    public CheckPingHttpConnection(String url, String request_Method) {
        this.url = url;
        this.request_Method = request_Method;

        try {

            //init. url
            URL mUrl = new URL(url);
            //init. connection
            conn = (HttpURLConnection) mUrl.openConnection();

            //setup configuration
            conn.setDoOutput(true);
            conn.setDoInput(false);
            conn.setUseCaches(false);
            //setup request method
            conn.setRequestMethod(request_Method);
            conn.setRequestProperty("Connection", "close");
            conn.setConnectTimeout(1000 * 30);
//            conn.setRequestProperty("Content-Type","text/plain");
//
            //setup authentication (in the configuration in the future)
//            String encoded_Authorization = new Authorization("1", "1")
//                    .getEncodedAuthorization_Base64();
            //setup request property
            //must use "Authorization", "Basic " + encode by base 64
//            conn.setRequestProperty("Authorization", "Basic " + encoded_Authorization);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("===\n" + e.getMessage() + "\n===");
        }
    }

    //getter & setter
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getRequest_Method() {
        return request_Method;
    }
    public void setRequest_Method(String request_Method) {
        this.request_Method = request_Method;
    }
    public DataOutputStream getDataOutputStream() {

        try {

            //init data output stream
            dataOutputStream = new DataOutputStream(conn.getOutputStream());
        } catch (IOException e) {

            //error messages
            e.printStackTrace();
            message = e.getMessage();
            System.out.print("===\nerror:" + e.getMessage() + "\n==");
        }

        return dataOutputStream;
    }
    public BufferedReader getBufferedReader() {

        try {

            //init input stream within buffer reader
            bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

        } catch (IOException e) {

            //error messages
            e.printStackTrace();
            message = e.getMessage();
            System.out.print("===\nerror:" + e.getMessage() + "\n==");
        }

        return bufferedReader;
    }
    public String getMessage() {
        return message;
    }
    //get connection
    public HttpURLConnection getConn() {
        return conn;
    }
    //get input stream
    public InputStream getConnInputStream() {

        try {
            return conn.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    //get status code & error message
    public String getConnStatusCodeAndErrorMessage() {

        try {
            return Integer.toString( conn.getResponseCode()) + "\n" + conn.getResponseMessage();
        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
    //get status code
    public String getConnStatusCode() {

        try {
            return Integer.toString(conn.getResponseCode());
        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
    //get error message
    public String getConnErrorMessage() {

        //init error message
        String eMessage =  "No Error.";
        try {

            if (conn.getResponseMessage() != null) {

                eMessage = conn.getResponseMessage();
            }

        } catch (IOException e) {

            //message
            e.printStackTrace();
            System.out.println("===\n" + e.getMessage() + "\n===");
        }

        return eMessage;
    }

    //other methods
    public void close() {

        //flush & close data output stream
        try {

            if (dataOutputStream != null) {

                dataOutputStream.flush();
                dataOutputStream.close();
            }
            if (bufferedReader != null) {

                bufferedReader.close();
            }

        } catch (IOException e) {

            //message
            e.printStackTrace();
            System.out.println("===\n" + e.getMessage() + "\n===");
        }

    }

}
