package pandaz.library.potingchiang.com.util.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.FrameLayout;

import pandaz.library.potingchiang.com.R;

import static android.view.WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
import static android.view.WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED;
import static android.view.WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;

/**
 * Copyright 2017 Poting Chiang
 * for api >= 23
 * This is to disable hardware key -> home, back and menu
 * Created by potingchiang on 2017-03-27.
 *
 * Copyright 2014 shaobin

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

// for locker
// init locker
// preference setting - using factory pattern
// boolean
//
public class HardKeyLocker {

    // init. overlay dialog
    private OverlayDialog overlayDialog;

    // lock
    public void lock(Activity activity) {

        if (Settings.canDrawOverlays(activity)) {

            if (overlayDialog == null) {

                overlayDialog = new OverlayDialog(activity);

                // show dialog
                overlayDialog.show();
            }

        }
        else {

            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
            activity.startActivity(intent);
        }
    }
    // unlock
    public void unlock() {

        if (overlayDialog != null) {

            // dismiss dialog
            overlayDialog.dismiss();
            // set dialog back to null, it will create new in next usage
            overlayDialog = null;
        }
    }

    // inner class
    private class OverlayDialog extends AlertDialog {

        public OverlayDialog(Activity activity) {
            super(activity, R.style.OverlayDialog);

            // setup window param
            WindowManager.LayoutParams params = getWindow().getAttributes();
            // setup new params with new values
            params.type = TYPE_SYSTEM_ERROR;
            // transparent
            params.dimAmount = 0.0F;
            params.width = 0;
            params.height = 0;
            params.gravity = Gravity.BOTTOM;
            // setup window with new params
            getWindow().setAttributes(params);
            // set mask to color code with 0xffffff
            getWindow().setFlags(FLAG_SHOW_WHEN_LOCKED | FLAG_NOT_TOUCH_MODAL, 0xffffff);
            // setup owner -> only belong to the owner
            setOwnerActivity(activity);
            // set cancel to false
            setCancelable(false);
        }

        // override
        @Override
        public final boolean dispatchTouchEvent(@NonNull MotionEvent ev) {

            return true;
        }
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            FrameLayout frameLayout = new FrameLayout(getContext());
            frameLayout.setBackgroundColor(0);
            setContentView(frameLayout);
        }
    }
}
