package pandaz.library.potingchiang.com.util.savingfiles;

/**
 *  This interface is for saving files in the Android
 * Created by potingchiang on 2017-05-20.
 */

public interface ISavingFiles {

    void openFileWrite(String content);
    String openFileRead();
}
