package pandaz.library.potingchiang.com.db;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * This is a factory for bd creation
 * Created by potingchiang on 2017-06-24.
 */

public abstract class PandaZDbHelper extends SQLiteOpenHelper {

    // key words
    // create table
    public static final String CREATE_TABLE                        = "CREATE TABLE ";
    public static final String CREATE_TABLE_NOT_EXIST              = "CREATE TABLE IF NOT EXISTS ";

    // drop table
    public static final String DROP_TABLE_EXIST                    = "DROP TABLE IF EXISTS ";

    // others
    public static final String OPENING                             = " ( ";
    public static final String CLOSING                             = " ) ";
    public static final String COMMA_SEP                           = ", ";
    public static final String SPACE                               = " ";

    // constraints
    public static final String NOT_NULL                            = " NOT NULL";
    public static final String CHECK                               = " CHECK";
    public static final String UNIQUE                              = " UNIQUE";

    // primary key
    // note: compound pk ->
    // primary key (columnA, columnB,....)
    public static final String PRIMARY_KEY_AUTO_STATEMENT          = " PRIMARY KEY AUTOINCREMENT";
    public static final String PRIMARY_KEY_INTEGER_AUTO_STATEMENT  = " INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final String PRIMARY_KEY                         = " PRIMARY KEY";
    public static final String PRIMARY_KEY_INTEGER                 = " INTEGER PRIMARY KEY";

    // foreign key
    // note:
    // foreign key is disable by default
    // need to enable with command line
    // basic ex:
    // FOREIGN KEY(columnB) REFERENCES tableA(columnA)
    // advanced ex:
    // https://sqlite.org/foreignkeys.html
    public static final String FOREIGN_KEY                         = " FOREIGN KEY";
    // reference
    public static final String REFERENCE                           = " REFERENCES";

    // data type
    public enum DataType {
        TEXT,
        INTEGER,
        REAL,
        BLOB
    }
//    public static final String TYPE_TEXT                           = " TEXT";
//    public static final String TYPE_INTEGER                        = " INTEGER";
//    public static final String TYPE_REAL                           = " REAL";
//    public static final String TYPE_BLOB                           = " BLOB";

    // constructor
    public PandaZDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                          int version) {
        super(context, name, factory, version);
    }
    public PandaZDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                          int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }
}
