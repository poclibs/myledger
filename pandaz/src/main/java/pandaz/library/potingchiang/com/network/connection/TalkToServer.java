package pandaz.library.potingchiang.com.network.connection;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * This class is to post to server and get response from server
 * Created by potingchiang on 2016-06-29.
 */
public class TalkToServer extends BasicHttpsConnection_Json {

//    //http connection
//    private BasicHttpConnection_Json connection_json;
    //url string
    private String url;
    //request method
    private String request_Method;
    //data output stream
    private DataOutputStream dataOutputStream;
    //buffer reader
    private BufferedReader bufferedReader;
    //status code
    private String connStatusCode;

    //constructor
    public TalkToServer(String url, String request_Method) {
        super(url, request_Method);

//        //init. connection
//        connection_json = new BasicHttpConnection_Json(url, request_Method);
        //assignment
//        this.connection_json = connection_json;
        this.url = url;
        this.request_Method = request_Method;

    }

    //post to server
    public String doPostAndGetResponse(JSONObject order_Json) {


        //init. response result
        String result = "";

        try {

//            connection_json.getConn().setDoOutput(false);
            //init data output stream
            if (getDataOutputStream() != null) {

                if (order_Json != null) {

                    //init json string
                    String order_Json_Str = order_Json.toString();

                    dataOutputStream = getDataOutputStream();
                    //write data into data output stream
                    dataOutputStream.writeBytes(order_Json_Str);

                    //dump request data
//                    System.out.println("===\nData output Json tring: " + order_Json_Str + "\n===");
                }
                else {

                    //error message
                    System.out.println("===\n" + "request_JSon is null!" + "\n===");
                }

            }
            else {

                //error message
                System.out.println("===\n" + "DataOutStream is null!" + "\n===");
//                Toast.makeText(this, "DataOutStream is null!", Toast.LENGTH_SHORT).show();
            }

            //status code
            //setup status code
            connStatusCode = getConnStatusCode();
//            Log.i("===\n" + "Status Code: \n", connection_json.getConnStatusCodeAndErrorMessage() + "\n===");
            System.out.println("===\n" + "Status code: \n" + getConnStatusCodeAndErrorMessage() + "\n===");
            //get response from server in the input stream
            String line;
            bufferedReader = getBufferedReader();
//            if (bufferedReader != null) {
            if (getConnInputStream() != null) {

                while ((line = bufferedReader.readLine()) != null) {

                    result += line +"\n";
                }
            }
            else {

                result = "-1";
            }

            //info from server
//            Toast.makeText(, result, Toast.LENGTH_LONG).show();
            System.out.println("===\nServer Json: " + result + "\n===");

        } catch (IOException e) {

            result = e.getMessage() ;
        }

        //clear, close stream and buffer reader
        close();

        return result;
    }

    //get from server
    public String doGetAndResponse() {


        //init. response result
        String result = "";

        try {

            //setup output false
            getConn().setDoOutput(false);
            //status code
            //setup status code
            connStatusCode = getConnStatusCode();
//            Log.i("===\n" + "Status Code: \n", connection_json.getConnStatusCodeAndErrorMessage() + "\n===");
            System.out.println("===\n" + "Status code: \n" + getConnStatusCodeAndErrorMessage() + "\n===");
            //get response from server in the input stream
            String line;
            bufferedReader = getBufferedReader();
//            if (bufferedReader != null) {
            if (getConnInputStream() != null) {

                while ((line = bufferedReader.readLine()) != null) {


                    result += line +"\n";
                }
            }
            else {

                result = "-1";
            }

            //info from server
            //dump request data
            System.out.println("===\nServer Json: " + result + "\n===");

        } catch (IOException e) {

            result = e.getMessage() ;
        }

        //clear, close stream and buffer reader
        close();

        return result;
    }

    //get img and transfer into byte array
    public byte[] getImgFromUrl_RESTful() {

        //concepts:
        //get/input data from input stream from server/url
        //put into array output stream and output to db or other target

        //init. byte[]
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        //need to check size of input stream
//            byte[] buffer = new byte[getConnInputStream().read()];
        byte[] buffer = new byte[1024];
        //check if input stream is not nll
        if (getConnInputStream() != null) {

            try {

                int len;
                while ((len = getConnInputStream().read()) != -1) {

                    outputStream.write(buffer, 0, len);
                }
            } catch (IOException e) {

                //error message
                e.printStackTrace();
                System.out.println("===\n" + e.getMessage() + "\n===");
            }

        }

        return outputStream.toByteArray();
    }
//    //get img from url - drawable
//    public Drawable getImgFromUrl_Drawable() {
//
//        //message
//        connStatusCode = getConnStatusCode();
////            Log.i("===\n" + "Status Code: \n", connection_json.getConnStatusCodeAndErrorMessage() + "\n===");
//        System.out.println("===\n" + "Status code: \n" + getConnStatusCodeAndErrorMessage() + "\n===");
//
//        return new Url_Handler(url).getImgFromUrl_Drawable();
//    }

}
